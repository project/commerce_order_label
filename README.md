## Commerce Order Label

Module essentially overrides label() on Commerce Order entity 
and adds in more logic to address Orders with empty labels.

### Requirements

Requires Commerce module & Commerce Order sub module.

https://www.drupal.org/project/commerce

### Configuration

No configuration is required. Simply install and enable the 
module.

### Maintainers

* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
